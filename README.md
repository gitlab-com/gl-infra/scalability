# Scalability Team

- 2020 Team Impact Overview: https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/764
- 2021 Team Impact Overview: https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/1473 

## Quick reference for how to find people 

We work with many people across the organization and it can be hard to know how to find people. 

### Are you looking for a contact in Infrastructure?

Most of the time we are looking for someone in Reliability. Reliability have specific teams for certain aspects:
- Observability
- Foundations
- Database
- Stable Counterparts

This is a recent change and the listing of who is working on which aspect can be found on this epic: https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/509

### Are you looking for a contact in Development or Product?

If you are looking for someone here, you are probably looking within the context of a stage group or feature category. 

Product sections, stages, groups and categories are listed here: https://about.gitlab.com/handbook/product/categories/.
You will also find the names of everyone involved in each feature category. 

If you want to see who that person reports to, you can use the org-chart https://comp-calculator.gitlab.net/org_chart to find their manager 
and job title. There is no single index of team-member locations, but you can find the location of someone using their profile on Slack. 

### Other

For finding other team members, the org-chart is helpful: https://comp-calculator.gitlab.net/org_chart.

You can also use the Slack channels to ping teams to look for the right contact. Keep in mind that there are channels with similar names where there is one for the concept (like #database) and one  for the groups (like #g_database). 

## Scaling Tools and Documentation

For Development Engineers
- Performance Guidelines: https://docs.gitlab.com/ee/development/performance.html
- Redis Development Guidelines: https://docs.gitlab.com/ee/development/redis.html
- Sidekiq Style Guide: https://docs.gitlab.com/ee/development/sidekiq_style_guide.html
- Feature Category attribution for Error Budgets: https://docs.gitlab.com/ee/development/feature_categorization/#rails-controllers
- Continuous Profiling for GitLab Go service: https://about.gitlab.com/handbook/engineering/monitoring/#go-services and https://www.youtube.com/watch?v=q3uudK1lU8g
- Dashboards for stage groups: https://docs.gitlab.com/ee/development/stage_group_dashboards.html and https://youtu.be/xB3gHlKCZpQ
- Gitaly - tour of periodic profiling: https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/1433#note_901717231
- Reference notes on Linux kernel memory pressure accounting (PSI): https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/1825 

For SREs
- Performance Monitoring: https://docs.gitlab.com/ee/administration/monitoring/performance/index.html
- Redis Runbook: https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/redis/redis.md
- Survival Guide for SREs to working with Redis at GitLab: https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/redis/redis-survival-guide-for-sres.md
- Survival Guide for SREs to working with Sidekiq at GitLab: https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/sidekiq/sidekiq-survival-guide-for-sres.md
- Client-side database connection pool settings: https://docs.gitlab.com/ee/development/database/client_side_connection_pool.html#client-side-connection-pool
- Redis Slowlog: https://youtu.be/BBI68QuYRH8
- Redis observability in Sidekiq logs: https://youtu.be/jw1Wv2IJxzs
- Sorting Rails log entries by number of Redis calls: https://youtu.be/Uhdj19Dc6vU
- User and IP Rate Limits: https://docs.gitlab.com/ee/user/admin_area/settings/user_and_ip_rate_limits.html
- Terraform basics: https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/2896#note_1864531963

Scalability Videos
- How we automatically generate recording rules from high cardinality metrics: https://www.youtube.com/watch?v=xh011z896w4
- Redis BigKeys analysis demo: https://www.youtube.com/watch?v=pinQTzPw8-o
- Go performance analysis using profiling: https://youtu.be/MxGQ7WZ2N74
- Optimizing application use of Redis: https://youtu.be/qgK8TPTZllU
- (Slide deck) Redis Bedtime Stories: https://speakerdeck.com/igorw/redis-bedtime-stories
- Mimir architecture: https://youtu.be/JxmPadNhT1I

[Slack Channel](https://gitlab.slack.com/messages/CMMF8TKR9)
| [Handbook Page](https://about.gitlab.com/handbook/engineering/infrastructure/team/scalability/)
| [Tracker](https://gitlab.com/gitlab-com/gl-infra/scalability/issues)
| [Google Drive Shared Folder](https://drive.google.com/drive/folders/1uIxU9auv7dxTRVc0AgZ9iw2BJlgjYDJK)
| [Onboarding Q&A](https://docs.google.com/document/d/1I9qEWExS3s2R_ir3ZsA58HaJoeS3vLe2T5cz7nNdvt0/edit?usp=sharing)
