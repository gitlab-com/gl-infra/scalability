# frozen_string_literal: true

RSpec.shared_examples "all task classes", type: :task_class do
  it "extends Runner" do
    expect(described_class).to be_kind_of(Runner)
    expect(described_class).to respond_to(:run)
  end
end
