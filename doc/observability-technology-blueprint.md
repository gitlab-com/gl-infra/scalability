# Scalability:Observability Technical Blueprint

This content has been moved into the handbook with https://gitlab.com/gitlab-com/content-sites/handbook/-/merge_requests/5970.

https://handbook.gitlab.com/handbook/engineering/infrastructure/team/scalability/observability/technical_blueprint.html
