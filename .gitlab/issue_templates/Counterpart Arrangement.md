<!---
 As Practices, we enable GitLab services to operate at production scale by providing paved roads for onboarding and maintaining features and services.
 Let us know how we can partner with you by providing the details below.
--->
## Request

### Requesting team

- Handbook: <!-- Link to a team page like https://handbook.gitlab.com/handbook/engineering/infrastructure/team/scalability/practices/ -->
- Slack Channel: <!-- Slack channel used to collaborate e.g #g_scalability-practices -->
- Engineering Manager: <!-- Who manages this team (gitlab handle) -->
- Label: <!-- team label like ~"team::scalability-practices" -->

### What kind of support are you looking for?

- [ ] Onboarding/Migration to [Runway](https://about.gitlab.com/direction/saas-platforms/scalability/runway/)
- [ ] [Production Readiness Review](https://handbook.gitlab.com/handbook/engineering/infrastructure/production/readiness/)
- [ ] SRE Support
- [ ] Production Change Request coordination 

### Describe the ongoing work that needs assistance

<!---
Add details about:
- Links to an epic with information about the project
- Details on priority of the work
- Expected due date
- DRI etc
--->

### Expectations for participating member(s) of the Scalability:Practices team

<!---
Explicitly outline what you expect the Practices team member to do and link respective issues.
--->

### Exit Criteria

<!---
Outline the criteria we should use to close this arrangement
In addition, we'll check in periodically and hold a retrospective on
how/if we should re-assess the arrangement.
--->

## Checklist

### Requesting Team
- [ ] The issue has a descriptive title
- [ ] There are detailed answers to the questions above
- [ ] The issue is assigned to the [Scalability:Practices EM](https://handbook.gitlab.com/handbook/engineering/infrastructure/team/scalability/practices/#team-members)
- [ ] If this is urgent, reach out to the engineering manager in slack

### Scalability:Practices Team
- [ ] There is enough information to prioritize the request
- [ ] The request has been assigned to a member of the team
- [ ] The priority of the request has been agreed by the stakeholders and author

/assign @kwanyangu
/label ~"group::scalability" ~"team::Scalability-Practices" ~"workflow-infra::Triage" 
/epic gitlab-com/gl-infra&1203 