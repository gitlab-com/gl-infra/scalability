#!/usr/bin/env bash

imageUrl="https://dashboards.gitlab.net/render/d-solo/general-mtbf/general-mean-time-between-failure?orgId=1&theme=light&var-PROMETHEUS_DS=Global&var-environment=gprd&var-stage=main&panelId=4&width=1000&height=500&tz=CET"

curl --silent -o global-gitlab-com-mtbf.png -H "Authorization: Bearer $GRAFANA_API_TOKEN" "${imageUrl}"
