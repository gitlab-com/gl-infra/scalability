# frozen_string_literal: true

require "shellwords"
require "open3"
require "json"
require "yaml"
require "net/http"

class Sync
  def execute
    raise "Project access token is missing" if project_access_token.nil? || project_access_token.empty?

    puts "Current local files, should include #{local_new_manifest_path}:"
    puts Dir.glob("*/*")
    puts "Downloading recent recent service maturity manifest from #{upstream_recent_manifest_url}"

    run("wget", "-q", upstream_recent_manifest_url, "-O", local_recent_manifest_path)

    new_manifest = read_file(
      local_new_manifest_path, <<~MSG
        Fail to read new manifest. This script should be run inside check-#{manifest_type} CI job which pulls the artifacts from the downstream job in the gitlab-com/runbooks"
      MSG
    )

    recent_manifest = read_file(
      local_recent_manifest_path, <<~MSG
        Fail to read the recent manifest downloaded from #{upstream_recent_manifest_url}"
      MSG
    )

    if new_manifest == recent_manifest
      puts "No changes detected. The #{manifest_type} manifest is up-to-date."
      return
    end

    puts "Changes detected. Start to sync..."

    diffs = diff_manifests(recent_manifest, new_manifest)
    puts diffs_description(diffs)
    branch = commit_new_manifest(new_manifest)
    create_mr(branch, diffs)
  end

  private

  def run(*command)
    cmd = Shellwords.join(command)
    puts "> #{cmd}"
    IO.popen(cmd, err: %i[child out]) { |io| puts io.read }
  end

  def commit_new_manifest(new_manifest)
    branch = generate_branch_name
    body = {
      branch: branch,
      commit_message: "Sync #{manifest_type} manifest",
      start_branch: mr_branch,
      actions: [
        {
          action: "update",
          file_path: upstream_manifest_path,
          content: new_manifest
        }
      ],
      force: true
    }

    puts "Pushing the latest manifest file..."
    response = Net::HTTP.post(
      URI("#{repo_api_url}/repository/commits"),
      body.to_json, headers
    )
    raise "Fail to create new commit: #{response.body}" unless success?(response)

    puts "Created new commit #{JSON.parse(response.body)["web_url"]} in branch #{branch}."
    branch
  end

  def create_mr(branch, diffs)
    body = {
      source_branch: branch,
      target_branch: mr_branch,
      draft: true,
      labels: "team::Scalability",
      title: mr_title,
      description: mr_description(diffs) + mr_description_footer,
      remove_source_branch: "true",
      merge_when_pipeline_succeeds: mr_auto_merge?
    }

    puts "Creating a new MR from #{branch} to master"
    response = Net::HTTP.post(
      URI("#{repo_api_url}/merge_requests"),
      body.to_json, headers
    )
    raise "Fail to create new MR: #{response.body}" unless success?(response)

    puts "Created new MR at #{JSON.parse(response.body)["web_url"]}"
  end

  def mr_auto_merge?
    false
  end

  def mr_description_footer
    return "" unless ENV["CI_JOB_URL"]

    "\n---\nTriggered by automation: #{ENV["CI_JOB_URL"]}"
  end

  def read_file(file_path, instruction)
    raise "#{instruction}\n\n#{file_path} does not exist" unless File.exist?(file_path)

    data = File.read(file_path)
    begin
      YAML.safe_load(data)
    rescue => e
      raise "#{instruction}\n\nFailed to load YAML:#{e.inspect}"
    end

    data
  end

  def generate_branch_name
    "scalability/sync-#{manifest_type}-manifest-#{Time.now.strftime("%Y-%m-%d")}"
  end

  def success?(response)
    code = response.code.to_i
    code >= 200 && code <= 204
  end

  def headers
    {
      "Content-Type" => "application/json",
      "Private-Token" => project_access_token
    }
  end

  def project_access_token
    ENV[project_access_token_name]
  end
end
