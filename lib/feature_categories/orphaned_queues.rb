# frozen_string_literal: true

require "yaml"

require_relative "../runner"

module FeatureCategories
  class OrphanedQueues
    extend Runner

    def execute
      generated_file = YAML.load_file("config/feature_categories.yml")
      repo_file = YAML.load_file("tmp/feature_categories.yml")
      all_queues = YAML.load_file("tmp/all_queues.yml") + YAML.load_file("tmp/ee_all_queues.yml")

      outdated_in_repo = repo_file - generated_file
      orphaned_queues =
        all_queues
          .select { |queue| outdated_in_repo.include?(queue[:feature_category].to_s) }
          .map { |queue| queue[:name] }

      return "Found no orphaned queues in config/feature_categories.yml!" if orphaned_queues.empty?

      raise <<~MESSAGE
        Orphaned queues found in config/feature_categories.yml!

        Outdated categories: #{outdated_in_repo.inspect}

        Orphaned queues: #{orphaned_queues.inspect}

        Please:
        1. Go to the gitlab-org/gitlab repository
        2. Run scripts/update-feature-categories
        3. Assign the orphaned queues to the correct categories and check with bundle exec rake gitlab:sidekiq:all_queues_yml:check
        4. Submit an MR
      MESSAGE
    end
  end
end
