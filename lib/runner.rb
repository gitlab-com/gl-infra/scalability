# frozen_string_literal: true

module Runner
  def run
    puts new.execute

    exit 0
  rescue => e
    warn e.message

    exit 1
  end
end
